const express    = require('express');
const fs         = require('fs');
const morgan     = require('morgan');
const lineByLine = require('n-readlines');
const path       = require('path');

// =============================== Constants ===================================
const TEXT_FILE_PATH   = path.join(__dirname, 'text.log');
const FILE_SIZE_PATH   = path.join(__dirname, 'size.log');
const PORT             = 3000;
const SIZE_OF_KB       = 1024;

// ============================ Class variables ================================
var app = express();

var textWriteStream = fs.createWriteStream(TEXT_FILE_PATH, {flags: 'a'});
var fileSizeStream  = fs.createWriteStream(FILE_SIZE_PATH, {flags: 'a'});

var liner    = new lineByLine(TEXT_FILE_PATH);
var story    = [];
var fileSize = 0;

// ============================ Class functions ================================
/**
 * Load text.log to memory and start the server
 * @param startServerCb the callback that starts the server
 */
var initServer = function(startServerCb){
    'use strict';
    var line;

    while (line = liner.next()) {
        var lineToAppend = line.toString('ascii') + '\n';
        story.push(lineToAppend);
        fileSize += lineToAppend.length;
    }
    updateFileSize();
    startServerCb();
};

/**
 * Update the file size in size.log according to fileSize
 */
var updateFileSize = function(){
    'use strict';
    var sizeInKB = fileSize / SIZE_OF_KB;

    fileSizeStream.write(sizeInKB.toString() + " \n", (err)=>{
        if (err){
            throw err;
        }
    });
};

/**
 * Log access via GET or POST in text.log
 * @param req the request from the user
 * @param res the respond to the user
 */
var logAccess = function(req, res){
    'use strict';
    var textToAppend = "";

    if (req.method === "GET" || req.method === "POST"){
        var realIp = req.get('X-Real-IP');
        textToAppend += realIp + " " +
            req.method + " " +
            req.get('User-Agent') + "\n";
        fileSize += textToAppend.length;
        updateFileSize();
        textWriteStream.write(textToAppend, (err)=>{
            if (err){
                throw err;
            }
        });
        story.push(textToAppend);
    }
};

// ========================== GET & POST logging ===============================
app.use(/^(.*)$/, function(req, res, next) {
    'use strict';
    logAccess(req, res);
    next();
});

//============================== Text logging ==================================

initServer(function(){
    app.listen(PORT, ()=>{
        'use strict';
        console.log("Server stared on port " + PORT);
    });
});

// PUT a line in text.log
app.put('/text', function(req, res){
    'use strict';
    var body = '';

    req.on('data', (data)=>{
        body += data;
    });
    req.on('end', ()=>{
        if (body){
            var jsonText = JSON.parse(body);
            var textToAppend = jsonText.text + '\n';
            fileSize += textToAppend.length;
            updateFileSize();
            textWriteStream.write(textToAppend, (err)=>{
                if (err){
                    throw err;
                }
            });
            story.push(textToAppend);
        }
        res.sendStatus(200).end();
    });

});

// GET rows from text.log
app.get('/text', function(req, res){
    'use strict';
    var results = "";
    var rows = req.query.rows;

    if (0 > rows || rows > story.length){
        res.status = 404;
        res.send("Illegal input\n");
        return;
    }
    for (var iLine = story.length - rows;
         iLine < story.length; iLine ++){
        results += story[iLine];
    }
    res.send(results);
});

// DELETE rows from text.log
app.delete('/text', function(req, res){
    'use strict';
    var rows = req.query.rows;

    if (0 > rows || rows > story.length){
        res.status = 404;
        res.send("Illegal input\n");
    } else if (0 == rows){
        res.sendStatus(200).end();
    } else {
        story.splice(story.length - rows);
        var length = 0;
        for (var iLine = 0; iLine < story.length; iLine ++){
            length += story[iLine].length;
        }
        fileSize = length;
        updateFileSize();
        fs.truncate(path.join(__dirname, TEXT_FILE_NAME),
            parseInt(length), function(err){
                if (err){
                    throw err;
                }
            });
        res.sendStatus(200).end();
    }
});

// Respond to all other requests
app.all(/^(.*)$/, function(req, res) {
    'use strict';
    res.sendStatus(200).end();
});

// =============================================================================
// ================================== EOF ======================================
// =============================================================================