***Ex3 - Monitoring with ELK***
========================================
Name: Yahav Itzhak

Username: yahavi

ID: 200615524

--


Name: Valeriya Zelikovich

Username: valeriya

ID: 332581172


General configuration information
=================================
Bluemix-VM

IP: 169.50.103.253
Kibana path: http://169.50.103.253/app/kibana


Files
=====
**app.js** - Implementation of part 2 of the exercise.

**README.md** - This file.


Part 1: Install elk stack
=========================
We installed Elk stack on a Bluemix VM.
Bluemix VMs not always allows us to connect to ports that are differ
from port 80. Hence we are using the reverse proxy service of Nginx.
It allows us to redirect /app/kibana data to port 5601 of kibana
and other data to port 3000.
At port 3000 lies our application from part 2.
Please don't use port explicitly since we are counting on Nginx.


Part 2: Server
==============
The server creates 2 files:
1. text.log - All of the required traces appear there.
2. size.log - The size of text.log.

**No blocking policy**
Since the size of text.log is no more than few MBs, we save an array of
all lines in RAM and destage it immediately after each operation. 

**GET & POST logging**
In order to trace each GET and POST, we wrote an handler that do this
job. 
*Usage: * Any GET or POST requests to 169.50.103.253/* will be logged
except Kibana users.

**Text file**
*Usage:*
To put "hello":
Put: curl -X PUT 169.50.103.253/text --header 
    "Content-Type:application/json" -d '{"text" : "hello"}'

To get the last 5 rows:
Get:    curl -X GET 169.50.103.253/text?rows=5

To delete the last 5 rows:
Delete: curl -X DELETE 169.50.103.253/text?rows=5


Part 3: System's parameters
===========================
We chose to show load and RAM. In order to collect the information
we use Collectd.


Part 4: Twitter
===============
We use the Logstash Twitter plugin. The 6 parameters are:
1. Jerusalem
2. Israel
3. Middle east
4. Asia
5. Earth
6. Solar system


Part 5: Kibana
==============
Use this link to enter Kibana:
http://169.50.103.253/app/kibana